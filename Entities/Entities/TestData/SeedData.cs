using RestoRunner.Entities;
public class SeedData
{
    public static void Initialize(RestoRunnerDbContext db)
    {
        // Добавление ролей
        var waiterRole = new Role { Name = "Официант", Description = "Роль официанта" };
        var managerRole = new Role { Name = "Менеджер", Description = "Роль менеджера" };
        var chefRole = new Role { Name = "Повар", Description = "Роль повара" };
        var customerRole = new Role { Name = "Клиент", Description = "Роль клиента" };
        db.Roles.AddRange(waiterRole, managerRole, chefRole, customerRole);
        db.SaveChanges();

        // Добавление пользователей
        var annaUser = new User { Username = "Anna", Password = "password123", Email = "anna@example.com", Role = waiterRole };
        var svetaUser = new User { Username = "Sveta", Password = "password456", Email = "sveta@example.com", Role = waiterRole };
        var artemUser = new User { Username = "Artem", Password = "password789", Email = "artem@example.com", Role = managerRole };
        var slavaUser = new User { Username = "Slava", Password = "passwordabc", Email = "slava@example.com", Role = chefRole };
        var ivanUser = new User { Username = "Ivan", Password = "passworddef", Email = "ivan@example.com", Role = customerRole };
        var mariaUser = new User { Username = "Maria", Password = "passwordghi", Email = "maria@example.com", Role = customerRole };
        var petrUser = new User { Username = "Petr", Password = "passwordjkl", Email = "petr@example.com", Role = customerRole };
        db.Users.AddRange(annaUser, svetaUser, artemUser, slavaUser, ivanUser, mariaUser, petrUser);
        db.SaveChanges();

        // Добавление категорий меню
        var appetizerCategory = new Category { Name = "Закуски", Description = "Категория закусок" };
        var mainDishCategory = new Category { Name = "Основные блюда", Description = "Категория основных блюд" };
        var dessertCategory = new Category { Name = "Десерты", Description = "Категория десертов" };
        db.Categories.AddRange(appetizerCategory, mainDishCategory, dessertCategory);
        db.SaveChanges();

        // Добавление ингредиентов
        var tomatoIngredient = new Ingredient { Name = "Томаты", Unit = "шт" };
        var onionIngredient = new Ingredient { Name = "Лук", Unit = "шт" };
        var potatoIngredient = new Ingredient { Name = "Картофель", Unit = "кг" };
        var beefIngredient = new Ingredient { Name = "Говядина", Unit = "кг" };
        var flourIngredient = new Ingredient { Name = "Мука", Unit = "кг" };
        var sugarIngredient = new Ingredient { Name = "Сахар", Unit = "кг" };
        db.Ingredients.AddRange(tomatoIngredient, onionIngredient, potatoIngredient, beefIngredient, flourIngredient, sugarIngredient);
        db.SaveChanges();

        // Добавление рецептов
        var caesarSaladRecipe = new Recipe { Name = "Салат Цезарь", Description = "Классический салат с курицей и сухариками", Instructions = "1. Нарежьте салат... 2. Обжарьте куриные грудки..." };
        var beefStroganoffRecipe = new Recipe { Name = "Бефстроганов", Description = "Блюдо из говядины с грибами и сметаной", Instructions = "1. Обжарьте говядину... 2. Обжарьте лук и грибы..." };
        var applePieRecipe = new Recipe { Name = "Яблочный пирог", Description = "Классический яблочный пирог с корицей", Instructions = "1. Замесите тесто... 2. Нарежьте яблоки..." };
        db.Recipes.AddRange(caesarSaladRecipe, beefStroganoffRecipe, applePieRecipe);
        db.SaveChanges();

        // Добавление блюд в меню
        var caesarSalad = new MenuItem { Name = "Салат Цезарь", Description = "Классический салат с курицей и сухариками", Price = 8.99m, Category = appetizerCategory};
        var beefStroganoff = new MenuItem { Name = "Бефстроганов", Description = "Блюдо из говядины с грибами и сметаной", Price = 14.99m, Category = mainDishCategory };
        var applePie = new MenuItem { Name = "Яблочный пирог", Description = "Классический яблочный пирог с корицей", Price = 6.99m, Category = dessertCategory };
        db.MenuItems.AddRange(caesarSalad, beefStroganoff, applePie);
        db.SaveChanges();

        Console.WriteLine("Данные успешно добавлены в базу данных.");
    }
}