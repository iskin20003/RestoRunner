using System.ComponentModel.DataAnnotations;

namespace RestoRunner.Entities
{
    public class SalesReport
    {
        [Key]
        public int Id { get; set; }
        public DateTime ReportDate { get; set; }
        public decimal TotalSales { get; set; }
        public int TotalOrders { get; set; }
    }
}