using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RestoRunner.Entities
{
    public class Ingredient
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Unit { get; set; }
        public ICollection<RecipeIngredient> RecipeIngredients { get; set; }
        public ICollection<Inventory> Inventory { get; set; }
    }
}