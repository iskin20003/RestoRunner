using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RestoRunner.Entities
{
    public class Recipe
    {
        [Key]
        public int Id { get; set; }
        public required string Name { get; set; }
        public required string Description { get; set; }
        public required string Instructions { get; set; }
        public ICollection<RecipeIngredient> RecipeIngredients { get; set; }
    }
}