using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestoRunner.Entities
{
    public class Employee
    {
        [Key]
        public int Id { get; set; }
        public required string Name { get; set; }
        public string? Email { get; set; }
        public required string Phone { get; set; }

        [ForeignKey("Role")]
        public int RoleId { get; set; }
        public required Role Role { get; set; }

        public ICollection<Shift> Shifts { get; set; }
    }
}