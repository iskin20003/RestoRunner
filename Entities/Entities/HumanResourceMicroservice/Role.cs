using System.ComponentModel.DataAnnotations;

namespace RestoRunner.Entities
{
    public class Role
    {
        [Key]
        public int Id { get; set; }
        public required string Name { get; set; }
        public required string Description { get; set; }
        public ICollection<Employee> Employees { get; set; }
        public ICollection<User> Users { get; set; }
    }
}