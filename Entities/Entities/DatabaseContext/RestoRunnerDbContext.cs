using Humanizer.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace RestoRunner.Entities
{
    public class RestoRunnerDbContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<MenuItem> MenuItems { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<RecipeIngredient> RecipeIngredients { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Inventory> Inventories { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<SalesReport> SalesReports { get; set; }
        public DbSet<PopularItem> PopularItems { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Shift> Shifts { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // Получаем конфигурацию из файла appsettings.json
            IConfiguration configuration = new ConfigurationBuilder()
                .AddJsonFile("./appsettings.json", optional: false, reloadOnChange: true)
                .Build();

            // string connectionString = string.Format(configuration.GetConnectionString("postgreSql"),
            //                                         configuration["PostgreSqlHost"],
            //                                         configuration["PostgreSqlPort"],
            //                                         configuration["PostgreSqlDatabase"],
            //                                         configuration["PostgreSqlUsername"],
            //                                         configuration["PostgreSqlPassword"]);

            string connectionString = configuration.GetConnectionString("DefaultConnection");

            // optionsBuilder.UseNpgsql("Server=localhost;Database=postgres;User Id=postgres;Password=Zx123456;");
            optionsBuilder.UseNpgsql(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MenuItem>()
                .HasOne(m => m.Category)
                .WithMany(c => c.MenuItems)
                .HasForeignKey(m => m.CategoryId);

            modelBuilder.Entity<OrderItem>()
                .HasOne(oi => oi.Order)
                .WithMany(o => o.OrderItems)
                .HasForeignKey(oi => oi.OrderId);

            modelBuilder.Entity<OrderItem>()
                .HasOne(oi => oi.MenuItem)
                .WithMany()
                .HasForeignKey(oi => oi.MenuItemId);

            modelBuilder.Entity<Order>()
                .HasOne(o => o.Customer)
                .WithMany(c => c.Orders)
                .HasForeignKey(o => o.CustomerId);

            modelBuilder.Entity<RecipeIngredient>()
                .HasKey(ri => new { ri.RecipeId, ri.IngredientId });

            modelBuilder.Entity<RecipeIngredient>()
                .HasOne(ri => ri.Recipe)
                .WithMany(r => r.RecipeIngredients)
                .HasForeignKey(ri => ri.RecipeId);

            modelBuilder.Entity<RecipeIngredient>()
                .HasOne(ri => ri.Ingredient)
                .WithMany(i => i.RecipeIngredients)
                .HasForeignKey(ri => ri.IngredientId);

            modelBuilder.Entity<Inventory>()
                .HasOne(i => i.Ingredient)
                .WithMany(i => i.Inventory)
                .HasForeignKey(i => i.IngredientId);

            modelBuilder.Entity<Payment>()
                .HasOne(p => p.Order)
                .WithMany()
                .HasForeignKey(p => p.OrderId);

            modelBuilder.Entity<PopularItem>()
                .HasOne(pi => pi.MenuItem)
                .WithMany()
                .HasForeignKey(pi => pi.MenuItemId);

            modelBuilder.Entity<Employee>()
                .HasOne(e => e.Role)
                .WithMany(r => r.Employees)
				.HasForeignKey(e => e.RoleId);

			modelBuilder.Entity<Shift>()
				.HasOne(s => s.Employee)
				.WithMany(e => e.Shifts)
				.HasForeignKey(s => s.EmployeeId);

			modelBuilder.Entity<User>()
				.HasOne(u => u.Role)
				.WithMany(r => r.Users)
				.HasForeignKey(u => u.RoleId);
		}
	}
}