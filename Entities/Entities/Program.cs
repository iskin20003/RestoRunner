﻿// using Entities.Context;

// using RestoRunner.Entities.DatabaseContext;

using RestoRunner.Entities;

namespace Entities
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new RestoRunnerDbContext())
            {
                // context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
                
                // Запишем тестовые данные в таблицы
                SeedData.Initialize(context);
            }
        }
    }
}