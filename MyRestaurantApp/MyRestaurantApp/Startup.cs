using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

// using MyRestaurantApp.Data;
// using RestoRunner.Entities;
using RestoRunner.Entities;
// using RestoRunner.Entities.AuthMicroservice;

namespace MyRestaurantApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            /*
            Здесь мы регистрируем контекст базы данных ApplicationDbContext и указываем, что будем использовать SQL Server в качестве поставщика базы данных. 
            Строка подключения к базе данных берется из конфигурационного файла (appsettings.json).
            */
            // services.AddDbContext<RestoRunnerDbContext>(options =>
            //     options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));
            services.AddDbContext<RestoRunnerDbContext>();

            /* 
            Эта строка регистрирует систему Identity для управления пользователями и ролями. 
            Мы указываем, что будем использовать нашу модель User вместо стандартной IdentityUser. 
            Также мы настраиваем, чтобы при входе в систему требовалось подтверждение учетной записи.
            */
            // services.AddIdentityCore<User>(options => options.SignIn.RequireConfirmedAccount = true)
            //     .AddEntityFrameworkStores<RestoRunnerDbContext>();

            // Эта строка регистрирует контроллеры и представления в конвейере запросов ASP.NET Core
            services.AddControllersWithViews();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            // Перенаправляет HTTP-запросы на HTTPS.
            // app.UseHttpsRedirection();
            // Обслуживает статические файлы (CSS, JavaScript, изображения).
            app.UseStaticFiles();
            // Добавляет маршрутизатор в конвейер запросов.
            app.UseRouting();

            // Добавляет компонент аутентификации в конвейер запросов.
            app.UseAuthentication();
            // Добавляет компонент авторизации в конвейер запросов.
            app.UseAuthorization();

            /*
            Эта часть кода настраивает маршрутизацию для контроллеров. Она определяет стандартный маршрут "{controller=Home}/{action=Index}/{id?}", 
            который будет использоваться для сопоставления URL-адресов с соответствующими действиями контроллеров.
            */
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}