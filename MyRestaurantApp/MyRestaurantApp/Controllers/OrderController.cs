using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
// using MyRestaurantApp.Data;
// using MyRestaurantApp.Models;
using RestoRunner.Entities;
// using RestoRunner.Entities.OrderMicroservice;
// using RestoRunner.Entities.AuthMicroservice;

namespace MyRestaurantApp.Controllers
{
    public class OrderController : Controller
    {
        private readonly RestoRunnerDbContext _context;

        public OrderController(RestoRunnerDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var userId = GetCurrentUserId(); // Get the current user's ID
            var orders = _context.Orders.Where(o => o.CustomerId == userId).ToList();
            return View(orders);
        }

        public IActionResult Create()
        {
            var menuItems = _context.MenuItems.ToList();
            return View(menuItems);
        }

        [HttpPost]
        public IActionResult Create(List<int> menuItemIds, List<int> quantities)
        {
            var userId = GetCurrentUserId(); // Get the current user's ID
            var order = new Order
            {
                CustomerId = userId,
                OrderDate = DateTime.Now,
                OrderItems = new List<OrderItem>()
            };

            decimal totalPrice = 0;
            for (int i = 0; i < menuItemIds.Count; i++)
            {
                var menuItem = _context.MenuItems.Find(menuItemIds[i]);
                var orderItem = new OrderItem
                {
                    MenuItemId = menuItemIds[i],
                    Quantity = quantities[i],
                    MenuItem = menuItem
                };
                order.OrderItems.Add(orderItem);
                totalPrice += menuItem.Price * quantities[i];
            }

            order.TotalPrice = totalPrice;
            _context.Orders.Add(order);
            _context.SaveChanges();

            return RedirectToAction("Payment", new { orderId = order.Id });
        }

        public IActionResult Payment(int orderId)
        {
            var order = _context.Orders.Include(o => o.OrderItems).ThenInclude(oi => oi.MenuItem).SingleOrDefault(o => o.Id == orderId);
            return View(order);
        }

        [HttpPost]
        public IActionResult Payment(int orderId, string paymentMethod)
        {
            // Process payment
            // ...

            return RedirectToAction("Success", new { orderId = orderId });
        }

        public IActionResult Success(int orderId)
        {
            var order = _context.Orders.Include(o => o.OrderItems).ThenInclude(oi => oi.MenuItem).SingleOrDefault(o => o.Id == orderId);
            return View(order);
        }

        private int GetCurrentUserId()
        {
            // Get the current user's ID from the authenticated user
            // ...
            return 1; // For example purposes
        }
    }
}