using Microsoft.AspNetCore.Mvc;

namespace RestaurantMenu.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}