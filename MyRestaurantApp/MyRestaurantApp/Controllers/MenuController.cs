using Microsoft.AspNetCore.Mvc;
// using MyRestaurantApp.Data;
using RestoRunner.Entities;

namespace MyRestaurantApp.Controllers
{
    public class MenuController : Controller
    {
        private readonly RestoRunnerDbContext _context;

        public MenuController(RestoRunnerDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var menuItems = _context.MenuItems.ToList();
            return View(menuItems);
        }
    }
}