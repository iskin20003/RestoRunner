using Microsoft.AspNetCore.Mvc;
// using MyRestaurantApp.Data;
// using MyRestaurantApp.Models;

using RestoRunner.Entities;
// using RestoRunner.Entities.AuthMicroservice;

namespace MyRestaurantApp.Controllers
{
    public class AccountController : Controller
    {
        private readonly RestoRunnerDbContext _context;

        public AccountController(RestoRunnerDbContext context)
        {
            _context = context;
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(string username, string password)
        {
            var user = _context.Users.SingleOrDefault(u => u.Username == username && u.Password == password);
            if (user != null)
            {
                // Authentificate user
                return RedirectToAction("Index", "Home");
            }
            else
            {
                // Show error message
                return View();
            }
        }

        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Register(User user)
        {
            _context.Users.Add(user);
            _context.SaveChanges();
            return RedirectToAction("Login");
        }
    }
}