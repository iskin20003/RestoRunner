using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace MyRestaurantApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        /*
        Метод CreateHostBuilder создает хост приложения с помощью Host.CreateDefaultBuilder. 
        Этот метод устанавливает некоторые настройки по умолчанию для хоста, такие как логирование и конфигурация.
        Затем вызывается метод ConfigureWebHostDefaults, который настраивает хост для веб-приложения. 
        В этом методе мы указываем, что класс Startup будет использоваться для настройки приложения с помощью метода UseStartup<Startup>.

        Класс Startup является важной частью приложения ASP.NET Core, где происходит настройка служб, конфигурации и конвейера запросов. 
        Он содержит два основных метода:
            ConfigureServices: здесь регистрируются все необходимые службы, такие как контекст базы данных, Identity, контроллеры и другие зависимости.
            Configure: в этом методе настраивается конвейер запросов HTTP, включая компоненты для обработки статических файлов, аутентификации, авторизации и маршрутизации.
        */
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}