using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

using MyRestaurantApp.Models;

// using System.Data.Common;

namespace MyRestaurantApp.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<MenuItem> MenuItems { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
    }
}