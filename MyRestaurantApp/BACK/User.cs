using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace MyRestaurantApp.Models
{
    public class User : IdentityUser<string>
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}