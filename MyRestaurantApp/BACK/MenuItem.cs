using System.Collections.Generic;

namespace MyRestaurantApp.Models
{
    public class MenuItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public ICollection<OrderItem> OrderItems { get; set; }
    }
}