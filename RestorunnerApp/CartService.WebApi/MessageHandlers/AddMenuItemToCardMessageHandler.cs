﻿using CartService.Services.Interfaces;
using Restorunner.MessageBroker.Messages.Abstractions;
using Restorunner.MessageBroker.Messages.Implementations;

namespace CartService.WebApi.MessageHandlers;

public class AddMenuItemToCardMessageHandler(ICartService cartService) : IMessageHandler<AddMenuItemToCartMessage>
{
    public async Task HandleAsync(AddMenuItemToCartMessage message)
    {
        await cartService.AddMenuItemToCart(message);
    }
}