﻿using System.Reflection;
using CartService.Contract;
using CartService.Infrastructure.Repositories.Implementations;
using CartService.Services.Interfaces;
using CartService.WebApi.MessageHandlers;
using Restorunner.Core.DAL.EntityFramework;
using Restorunner.Core.DAL.Repositories.Abstractions;
using Restorunner.MessageBroker.Bus;
using Restorunner.MessageBroker.Messages.Implementations;
using Restorunner.MessageBroker.RabbitMq;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using CartItem = Restorunner.Core.DAL.Entities.CartService.CartItem;

namespace CartService.WebApi;

public class Startup(IConfiguration configuration)
{
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddDbContext<DatabaseContext>(options =>
        {
            options.UseNpgsql(configuration.GetConnectionString("DefaultConnection"));
        });
        services
            .AddScoped<IRepository<CartItem, long>, CartItemRepository>()
            .AddScoped<ICartService, Services.Implementations.CartService>()
            .AddMapping()
            .AddControllers();

        services.AddSwaggerGen(options =>
        {
            options.SwaggerDoc("v1",
                new OpenApiInfo
                {
                    Title = "Cart Service API",
                    Version = "v1"
                });

            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
            options.IncludeXmlComments(xmlPath);
        });

        AddMessageBroker(services);
    }

    private void AddMessageBroker(IServiceCollection services)
    {
        var sectionRabbitMq = configuration.GetSection("RabbitMq");

        var host = sectionRabbitMq.GetSection("Host").Value;
        var exchange = sectionRabbitMq.GetSection("Exchange").Value;
        var queue = sectionRabbitMq.GetSection("Queue").Value;

        services.AddRabbitMqMessageBus(host, exchange, queue)
            .AddTransient<AddMenuItemToCardMessageHandler>();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }

        app.UseRouting();
        app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

        app.UseSwagger();
        app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "Cart API V1"); });

        var messageBus = app.ApplicationServices.GetService<IMessageBus>();
        messageBus!.Subscribe<AddMenuItemToCartMessage, AddMenuItemToCardMessageHandler>();
    }
}