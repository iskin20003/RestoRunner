﻿using CartService.Contract.Contract;
using CartService.Services.Exceptions;
using CartService.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CartService.WebApi.Controllers;

[Route("api/[controller]")]
[ApiController]
public class CartController(ICartService cartService) : ControllerBase
{
    /// <summary>
    /// Получить список элементов в корзине
    /// </summary>
    /// <param name="clientName"></param>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<IEnumerable<CartItemDto>>> GetAllCartItem([FromQuery] string clientName)
    {
        var result = await cartService.GetMenuItemsToCart(clientName);

        return Ok(result);
    }

    [HttpDelete]
    public async Task<IActionResult> DeleteMenuItemFromCart(long cartItemId)
    {
        try
        {
            await cartService.DeleteMenuItemToCart(cartItemId);
        }
        catch (CartItemNotFoundException e)
        {
            return BadRequest(e.Message);
        }

        return Ok();
    }

    [HttpPost("increment-{cartItemId}")]
    public async Task<IActionResult> IncrementCartItem(long cartItemId)
    {
        try
        {
            await cartService.IncrementMenuItemToCart(cartItemId);
        }
        catch (CartItemNotFoundException e)
        {
            return BadRequest(e.Message);
        }

        return Ok();
    }

    [HttpPost("decrement-{cartItemId}")]
    public async Task<IActionResult> DecrementCartItem(long cartItemId)
    {
        try
        {
            await cartService.DecrementMenuItemToCart(cartItemId);
        }
        catch (CartItemNotFoundException e)
        {
            return BadRequest(e.Message);
        }

        return Ok();
    }

    [HttpPost]
    public async Task<IActionResult> CreateOrder(string clientName)
    {
        try
        {
            await cartService.CreateOrder(clientName);
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }

        return Ok("Заказ создан");
    }
}