﻿using Restorunner.Core.DAL.Entities.OrderService;
using Restorunner.Core.DAL.EntityFramework;
using Restorunner.Core.DAL.Repositories.Abstractions;

namespace OrderService.Infrastructure.Repositories.Implementations;

public class OrderRepository(DatabaseContext context) : Repository<Order, long>(context)
{
    
}