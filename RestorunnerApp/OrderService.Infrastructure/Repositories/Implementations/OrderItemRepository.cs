﻿using Restorunner.Core.DAL.Entities.OrderService;
using Restorunner.Core.DAL.EntityFramework;
using Restorunner.Core.DAL.Repositories.Abstractions;

namespace OrderService.Infrastructure.Repositories.Implementations;

public class OrderItemRepository(DatabaseContext context) : Repository<OrderItem, long>(context)
{
    
}