﻿using AutoMapper;
using MenuService.Contract.Mappings;
using Microsoft.Extensions.DependencyInjection;

namespace MenuService.Contract;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddMapping(this IServiceCollection services)
    {
        services.AddSingleton<IMapper>(new Mapper(ConfigureMappings()));

        return services;
    }

    private static IConfigurationProvider ConfigureMappings()
    {
        var config = new MapperConfiguration(config =>
        {
            config.AddProfile<MenuItemProfile>();
        });

        config.AssertConfigurationIsValid();
        return config;
    }
}