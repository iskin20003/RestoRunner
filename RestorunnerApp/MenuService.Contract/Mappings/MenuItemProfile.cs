﻿using AutoMapper;
using MenuService.Contract.Contract;
using Restorunner.Core.DAL.Entities.MenuService;

namespace MenuService.Contract.Mappings;

public class MenuItemProfile : Profile
{
    public MenuItemProfile()
    {
        CreateMap<MenuItem, MenuItemDto>().ReverseMap();
    }
}