﻿namespace MenuService.Contract.Contract;

public class MenuItemDto
{
    public long Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public decimal Price { get; set; }
}