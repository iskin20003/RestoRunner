﻿using MenuService.Contract.Contract;

namespace MenuService.Services.Interfaces;

public interface IMenuItemService
{
    Task<IEnumerable<MenuItemDto>> GetMenuItemsAsync();
    Task<MenuItemDto?> GetMenuItemByIdAsync(long id);
    Task<MenuItemDto> AddMenuItemAsync(MenuItemDto menuItem);
    Task<MenuItemDto> UpdateMenuItemAsync(MenuItemDto menuItem);
    Task DeleteMenuItemAsync(long id);
}