﻿using AutoMapper;
using MenuService.Contract.Contract;
using MenuService.Services.Interfaces;
using Restorunner.Core.DAL.Entities.MenuService;
using Restorunner.Core.DAL.Repositories.Abstractions;

namespace MenuService.Services.Implementation;

public class MenuItemService(IRepository<MenuItem, long> repository, IMapper mapper) : IMenuItemService
{
    public async Task<IEnumerable<MenuItemDto>> GetMenuItemsAsync()
    {
        var menuItems = await repository.GetAll();
        List<MenuItemDto> menuItemsDto = [];
        menuItemsDto.AddRange(menuItems.Select(mapper.Map<MenuItemDto>));

        return menuItemsDto;
    }

    public async Task<MenuItemDto> GetMenuItemByIdAsync(long id)
    {
        var menuItem = await repository.Get(id);
        return menuItem == null ? null : mapper.Map<MenuItemDto>(menuItem);
    }

    public async Task<MenuItemDto> AddMenuItemAsync(MenuItemDto menuItem)
    {
        var menuItemEntity = mapper.Map<MenuItem>(menuItem);

        var id = await repository.Add(menuItemEntity);

        menuItem.Id = id;

        return menuItem;
    }

    public async Task<MenuItemDto> UpdateMenuItemAsync(MenuItemDto menuItem)
    {
        var menuItemEntity = mapper.Map<MenuItem>(menuItem);

        await repository.Update(menuItemEntity);

        return menuItem;
    }

    public async Task DeleteMenuItemAsync(long id)
    {
        await repository.DeleteById(id);
    }
}