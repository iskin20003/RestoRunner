﻿using Restorunner.Core.DAL.Entities.KitchenService;
using Restorunner.Core.DAL.EntityFramework;
using Restorunner.Core.DAL.Repositories.Abstractions;

namespace KitchenService.Infrastructure.Repositories.Implementations;

public class CookRepository(DatabaseContext context) : Repository<Cook, long>(context)
{
    
}