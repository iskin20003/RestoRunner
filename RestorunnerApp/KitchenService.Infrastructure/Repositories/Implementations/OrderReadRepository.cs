﻿using Restorunner.Core.DAL.Entities.OrderService;
using Restorunner.Core.DAL.EntityFramework;
using Restorunner.Core.DAL.Repositories.Abstractions;

namespace KitchenService.Infrastructure.Repositories.Implementations;

public class OrderReadRepository(DatabaseContext context) : ReadRepository<Order, long>(context)
{
    
}