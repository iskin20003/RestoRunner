﻿using Restorunner.Core.DAL.Entities.Enums;
using Restorunner.MessageBroker.Messages.Abstractions;

namespace Restorunner.MessageBroker.Messages.Implementations;

public class ChangeStatusOrderMessage(long orderId, OrderStatus newOrderStatus) : Message
{
    public long OrderId { get; init; } = orderId;

    public OrderStatus NewOrderStatus { get; init; } = newOrderStatus;
}