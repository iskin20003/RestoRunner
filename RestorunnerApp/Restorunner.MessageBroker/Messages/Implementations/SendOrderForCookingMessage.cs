﻿using Restorunner.MessageBroker.Messages.Abstractions;

namespace Restorunner.MessageBroker.Messages.Implementations;

public class SendOrderForCookingMessage(long orderId) : Message
{
    public long OrderId { get; init; } = orderId;
}