﻿using Restorunner.MessageBroker.Messages.Abstractions;

namespace Restorunner.MessageBroker.Messages.Implementations;

public class AddMenuItemToCartMessage(string menuItemName, int quantity, string clientName, decimal menuItemPrice) : Message
{
    public string MenuItemName { get; } = menuItemName;
    public int Quantity { get; } = quantity;
    public string ClientName { get; } = clientName;
    public decimal MenuItemPrice { get; } = menuItemPrice;
}