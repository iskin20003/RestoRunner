﻿using Restorunner.MessageBroker.Messages.Abstractions;

namespace Restorunner.MessageBroker.Messages.Implementations;

public class CreateOrderMessage : Message
{
    public string ClientName { get; set; }
    public ICollection<CartItem> CartItems { get; set; }
}

public class CartItem
{
    public string MenuItemName { get; set; }
    public int Quantity { get; set; }
    public decimal Price { get; set; }
}