﻿namespace Restorunner.MessageBroker.Messages.Abstractions;

public abstract class Message
{
    public Guid Id { get; set; } = Guid.NewGuid();
    public DateTime CreateMessage { get; set; } = DateTime.Now;
}