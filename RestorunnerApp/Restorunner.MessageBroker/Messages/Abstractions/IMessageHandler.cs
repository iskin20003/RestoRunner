﻿namespace Restorunner.MessageBroker.Messages.Abstractions;

public interface IMessageHandler<in TMessage> where TMessage : Message
{
    Task HandleAsync(TMessage message);
}