﻿using Restorunner.MessageBroker.Messages.Abstractions;

namespace Restorunner.MessageBroker.Subscribtions;

public class InMemoryMessageBusSubscriptionManager : IMessageBusSubscriptionManager
{
    private readonly Dictionary<string, List<Subscription>> _handlers = new();
    private readonly List<Type> _messagesTypes = new();

    public bool IsEmpty => _handlers.Keys.Count == 0;

    public event EventHandler<string> OnMessageRemovedHandler;

    #region Override methods

    public void AddSubscription<TMessage, TMessageHandler>()
        where TMessage : Message
        where TMessageHandler : IMessageHandler<TMessage>
    {
        var messageName = GetMessageIndentifier<TMessage>();

        DoAddSubscription(typeof(TMessage), typeof(TMessageHandler), messageName);

        if (!_messagesTypes.Contains(typeof(TMessage)))
            _messagesTypes.Add(typeof(TMessage));
    }

    public void RemoveSubscription<TMessage, TMessageHandler>()
        where TMessage : Message
        where TMessageHandler : IMessageHandler<TMessage>
    {
        var handlerToRemove = FindSubscriptionToRemove<TMessage, TMessageHandler>();
        var messageName = GetMessageIndentifier<TMessage>();
        DoRemoveHandler(messageName, handlerToRemove);
    }

    public void Clear()
    {
        _handlers.Clear();
        _messagesTypes.Clear();
    }

    public Dictionary<string, List<Subscription>> GetAllSubscriptions()
    {
        return new Dictionary<string, List<Subscription>>(_handlers);
    }

    public string GetMessageIndentifier<TMessage>()
    {
        return typeof(TMessage).Name;
    }

    public Type? GetMessageTypeByName(string messageName)
    {
        return _messagesTypes.FirstOrDefault(t => t.Name == messageName);
    }

    public IEnumerable<Subscription> GetSubscriptionsForMessage(string messageName)
    {
        return _handlers[messageName];
    }

    public bool HasSubscriptionForMessage(string messageName)
    {
        return _handlers.ContainsKey(messageName);
    }

    #endregion

    #region Methods

    private void DoAddSubscription(Type messageType, Type messageHandlerType, string messageName)
    {
        if (!HasSubscriptionForMessage(messageName))
            _handlers.Add(messageName, new List<Subscription>());

        if (_handlers[messageName].Any(x => x.HandlerType == messageHandlerType))
            throw new ArgumentException($"ТИп обработчика {messageHandlerType} уже зарегистрирован для сообщений {messageName}", nameof(messageHandlerType));

        _handlers[messageName].Add(new Subscription(messageType, messageHandlerType));
    }

    private void DoRemoveHandler(string messageName, Subscription? handlerToRemove)
    {
        if (handlerToRemove == null)
            return;

        _handlers[messageName].Remove(handlerToRemove);

        if (_handlers[messageName].Count > 0)
            return;

        _handlers.Remove(messageName);

        var messageType = _messagesTypes.FirstOrDefault(t => t.Name == messageName);
        if (messageType != null)
            _messagesTypes.Remove(messageType);

        OnMessageRemovedHandler?.Invoke(this, messageName);
    }

    private Subscription? FindSubscriptionToRemove<TMessage, TMessageHandler>()
        where TMessage : Message
        where TMessageHandler : IMessageHandler<TMessage>
    {
        var messageName = GetMessageIndentifier<TMessage>();
        return DoFindSubscriptionToRemove(messageName, typeof(TMessageHandler));
    }

    private Subscription? DoFindSubscriptionToRemove(string messageName, Type handlerType)
    {
        if (!HasSubscriptionForMessage(messageName))
            return null;

        return _handlers[messageName].FirstOrDefault(s => s.HandlerType == handlerType);
    }

    #endregion
}