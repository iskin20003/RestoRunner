﻿namespace Restorunner.MessageBroker.Subscribtions;

public class Subscription(Type messageType, Type handlerType)
{
    public Type MessageType { get; private set; } = messageType;

    public Type HandlerType { get; private set; } = handlerType;
}