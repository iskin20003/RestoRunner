﻿using Restorunner.MessageBroker.Messages.Abstractions;

namespace Restorunner.MessageBroker.Subscribtions;

public interface IMessageBusSubscriptionManager
{
    event EventHandler<string> OnMessageRemovedHandler;

    bool IsEmpty { get; }

    bool HasSubscriptionForMessage(string messageName);

    string GetMessageIndentifier<TMessage>();

    Type? GetMessageTypeByName(string messageName);

    IEnumerable<Subscription> GetSubscriptionsForMessage(string messageName);

    Dictionary<string, List<Subscription>> GetAllSubscriptions();

    void AddSubscription<TMessage, TMessageHandler>()
        where TMessage : Message
        where TMessageHandler : IMessageHandler<TMessage>;

    void RemoveSubscription<TMessage, TMessageHandler>()
        where TMessage : Message
        where TMessageHandler : IMessageHandler<TMessage>;

    void Clear();
}