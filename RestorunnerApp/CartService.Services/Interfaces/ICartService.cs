﻿using CartService.Contract.Contract;
using Restorunner.MessageBroker.Messages.Implementations;

namespace CartService.Services.Interfaces;

public interface ICartService
{
    Task IncrementMenuItemToCart(long cartId);

    Task DecrementMenuItemToCart(long cartId);

    Task<ICollection<CartItemDto>> GetMenuItemsToCart(string clientName);

    Task DeleteMenuItemToCart(long cartId);

    Task CreateOrder(string clientName);

    Task<CartItemDto> AddMenuItemToCart(AddMenuItemToCartMessage message);
}