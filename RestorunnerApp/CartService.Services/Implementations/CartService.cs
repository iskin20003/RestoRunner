﻿using AutoMapper;
using CartService.Contract.Contract;
using CartService.Services.Exceptions;
using CartService.Services.Interfaces;
using Restorunner.Core.DAL.Repositories.Abstractions;
using Restorunner.MessageBroker.Bus;
using Restorunner.MessageBroker.Messages.Implementations;
using CartItem = Restorunner.Core.DAL.Entities.CartService.CartItem;

namespace CartService.Services.Implementations;

public class CartService(IRepository<CartItem, long> repository, IMapper mapper, IMessageBus messageBus) : ICartService
{
    public async Task IncrementMenuItemToCart(long cartId)
    {
        var cartItem = await repository.Get(cartId);
        if (cartItem == null)
            throw new CartItemNotFoundException("Не найден элемент с таким номером.");

        var oneElementPrice = cartItem.Price / cartItem.Quantity;
        cartItem.Quantity++;
        cartItem.Price = cartItem.Quantity * oneElementPrice;

        await repository.Update(cartItem);
    }

    public async Task DecrementMenuItemToCart(long cartId)
    {
        var cartItem = await repository.Get(cartId);
        if (cartItem == null)
            throw new CartItemNotFoundException("Не найден элемент с таким номером.");

        var oneElementPrice = cartItem.Price / cartItem.Quantity;
        cartItem.Quantity--;
        if (cartItem.Quantity == 0)
        {
            await repository.Delete(cartItem);
            return;
        }
        cartItem.Price = cartItem.Quantity * oneElementPrice;

        await repository.Update(cartItem);
    }

    public async Task<ICollection<CartItemDto>> GetMenuItemsToCart(string clientName)
    {
        var listCartItem = await repository.GetEntitiesByCustomQuery(x => x.ClientName == clientName);

        var result = listCartItem.Select(mapper.Map<CartItem, CartItemDto>).ToList();

        return result;
    }

    public async Task DeleteMenuItemToCart(long cartId)
    {
        var cartItem = await repository.Get(cartId);
        if (cartItem == null)
            throw new CartItemNotFoundException("Не найден элемент с таким номером.");

        await repository.Delete(cartItem);
    }

    public async Task CreateOrder(string clientName)
    {
        var cartItems = await GetMenuItemsToCart(clientName);

        var createCartItems = cartItems.Select(x => new Restorunner.MessageBroker.Messages.Implementations.CartItem
            { MenuItemName = x.MenuItemName, Price = x.Price, Quantity = x.Quantity }).ToList();

        var createOrderMessage = new CreateOrderMessage
        {
            ClientName = clientName,
            CartItems = createCartItems
        };

        messageBus.Publish(createOrderMessage);
    }

    public async Task<CartItemDto> AddMenuItemToCart(AddMenuItemToCartMessage message)
    {
        var cartItem = new CartItemDto
        {
            MenuItemName = message.MenuItemName,
            Quantity = message.Quantity,
            ClientName = message.ClientName,
        };
        cartItem.Price = cartItem.Quantity * message.MenuItemPrice;

        cartItem.Id = await repository.Add(mapper.Map<CartItemDto, CartItem>(cartItem));

        return cartItem;
    }
}