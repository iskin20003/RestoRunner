﻿namespace CartService.Services.Exceptions;

public class CartItemNotFoundException(string message) : Exception(message)
{
    
}