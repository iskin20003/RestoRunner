﻿using Restorunner.Core.DAL.Entities;
using System.Linq.Expressions;

namespace Restorunner.Core.DAL.Repositories.Abstractions;

public interface IWriteRepository<TEntity, TKey> 
    where TKey : struct
    where TEntity : IEntity<TKey>
{
    Task<TKey> Add(TEntity entity);
    Task AddRange(IEnumerable<TEntity> entities);
    Task<bool> Delete(TEntity entity);
    Task<bool> DeleteById(TKey keyId);
    Task<bool> DeleteRange(IEnumerable<TEntity> entities);
    Task<int> DeleteWithPredicate(Expression<Func<TEntity, bool>> predicate);
    Task<bool> Update(TEntity entity);
}