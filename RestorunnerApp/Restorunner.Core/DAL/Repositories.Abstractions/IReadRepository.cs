﻿using Restorunner.Core.DAL.Entities;
using System.Linq.Expressions;

namespace Restorunner.Core.DAL.Repositories.Abstractions;

public interface IReadRepository<TEntity, in TKey>
    where TKey : struct
    where TEntity : IEntity<TKey>
{
    Task<TEntity?> Get(TKey keyId);
    Task<IEnumerable<TEntity>> GetAll(bool asNoTracking = false);
    Task<IEnumerable<TEntity>> GetEntitiesByCustomQuery(Expression<Func<TEntity, bool>> predicate);
}