﻿using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Restorunner.Core.DAL.Entities;
using Restorunner.Core.DAL.EntityFramework;

namespace Restorunner.Core.DAL.Repositories.Abstractions;

public abstract class Repository<TEntity, TKey>(DatabaseContext dbContext)
    : ReadRepository<TEntity, TKey>(dbContext), IRepository<TEntity, TKey>
    where TKey : struct
    where TEntity : class, IEntity<TKey>
{
    public async Task<TKey> Add(TEntity entity)
    {
        var result = await Set.AddAsync(entity).ConfigureAwait(false);
        await dbContext.SaveChangesAsync().ConfigureAwait(false);
        return result.Entity.Id;
    }

    public async Task AddRange(IEnumerable<TEntity> entities)
    {
        await Set.AddRangeAsync(entities).ConfigureAwait(false);
        await dbContext.SaveChangesAsync().ConfigureAwait(false);
    }

    public async Task<bool> Delete(TEntity entity)
    {
        var obj = await Set.FindAsync(entity.Id).ConfigureAwait(false);

        if (obj == null) return false;

        var result = await Set.Where(x => x.Id.Equals(obj.Id)).ExecuteDeleteAsync().ConfigureAwait(false);
        
        await dbContext.SaveChangesAsync().ConfigureAwait(false);
        
        return result == 1;
    }

    public  async Task<bool> DeleteById(TKey keyId)
    {
        var obj = await Set.FindAsync(keyId).ConfigureAwait(false);

        if (obj == null) return false;

        var result = await Set.Where(x => x.Id.Equals(obj.Id)).ExecuteDeleteAsync().ConfigureAwait(false);

        await dbContext.SaveChangesAsync().ConfigureAwait(false);

        return result == 1;
    }

    public async Task<bool> DeleteRange(IEnumerable<TEntity> entities)
    {
        if (entities == null || !entities.Any())
            return false;
        foreach (var entity in entities)
        {
            await Set.Where(x => x.Id.Equals(entity.Id)).ExecuteDeleteAsync().ConfigureAwait(false);
        }

        await dbContext.SaveChangesAsync().ConfigureAwait(false);

        return true;
    }

    public async Task<int> DeleteWithPredicate(Expression<Func<TEntity, bool>> predicate)
    {
        if (predicate == null)
        {
            throw new ArgumentException(nameof(predicate));
        }

        await dbContext.SaveChangesAsync().ConfigureAwait(false);

        return await Set.Where(predicate).ExecuteDeleteAsync();
    }

    public async Task<bool> Update(TEntity entity)
    {
        Set.Update(entity);

        await dbContext.SaveChangesAsync().ConfigureAwait(false);

        return true;
    }

    public async Task SaveChangesAsync(CancellationToken token = default)
    {
        await Context.SaveChangesAsync(token).ConfigureAwait(false);
    }
}