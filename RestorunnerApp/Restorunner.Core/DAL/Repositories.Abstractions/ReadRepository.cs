﻿using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Restorunner.Core.DAL.Entities;
using Restorunner.Core.DAL.EntityFramework;

namespace Restorunner.Core.DAL.Repositories.Abstractions;

public abstract class ReadRepository<TEntity, TKey> : IReadRepository<TEntity, TKey>
    where TKey : struct
    where TEntity : class, IEntity<TKey>
{
    protected readonly DatabaseContext Context;
    protected DbSet<TEntity> Set;

    protected ReadRepository(DatabaseContext dbContext)
    {
        Context = dbContext;
        Set = Context.Set<TEntity>();
    }

    public async Task<TEntity?> Get(TKey keyId)
    {
        return await Set.FindAsync(keyId).ConfigureAwait(false);
    }

    public async Task<IEnumerable<TEntity>> GetAll(bool asNoTracking = false)
    {
        return await(asNoTracking ? Set.AsNoTracking() : Set).ToListAsync();
    }

    public async Task<IEnumerable<TEntity>> GetEntitiesByCustomQuery(Expression<Func<TEntity, bool>> predicate)
    {
        return await Set.Where(predicate).ToListAsync();
    }
}