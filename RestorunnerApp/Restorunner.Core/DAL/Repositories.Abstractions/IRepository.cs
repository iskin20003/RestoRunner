﻿using Restorunner.Core.DAL.Entities;

namespace Restorunner.Core.DAL.Repositories.Abstractions;

public interface IRepository<T, TPrimaryKey> : IReadRepository<T, TPrimaryKey>, IWriteRepository<T, TPrimaryKey>
    where TPrimaryKey : struct
    where T : IEntity<TPrimaryKey>
{
    Task SaveChangesAsync(CancellationToken token = default);
}