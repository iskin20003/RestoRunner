﻿using Restorunner.Core.DAL.Entities;
using Restorunner.Core.DAL.Entities.ClientInfo;
using Restorunner.Core.DAL.Entities.Enums;

namespace Restorunner.Core.DAL.Entities.OrderService;

public class Order : IEntity<long>
{
    public long Id { get; set; }

    public string ClientName { get; set; }

    public ICollection<OrderItem> OrderItems { get; set; }

    public DateTimeOffset CreateOrder { get; set; }

    public OrderStatus Status { get; set; }

    public OrderType OrderType { get; set; }

    public PaymentMethod PaymentMethod { get; set; }

    public decimal TotalPrice => OrderItems.Sum(orderItem => orderItem.TotalPrice);
}