﻿using Restorunner.Core.DAL.Entities;
using Restorunner.Core.DAL.Entities.MenuService;

namespace Restorunner.Core.DAL.Entities.OrderService;

public class OrderItem : IEntity<long>
{
    public long Id { get; set; }

    public long OrderId { get; set; }
    public Order Order { get; set; }

    public string MenuItemName { get; set; }

    public int Quantity { get; set; }

    public decimal TotalPrice { get; set; }
}