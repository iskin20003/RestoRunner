﻿namespace Restorunner.Core.DAL.Entities;

/// <summary>
/// Общий интерфейс для всех сущностей.
/// </summary>
/// <typeparam name="TId">Ид.</typeparam>
public interface IEntity<TId> where TId : struct
{
    TId Id { get; set; }
}