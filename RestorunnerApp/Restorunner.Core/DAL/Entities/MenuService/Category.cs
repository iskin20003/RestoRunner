﻿using Restorunner.Core.DAL.Entities;

namespace Restorunner.Core.DAL.Entities.MenuService;

public class Category : IEntity<long>
{
    public long Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public ICollection<MenuItem> MenuItems { get; set; }
}