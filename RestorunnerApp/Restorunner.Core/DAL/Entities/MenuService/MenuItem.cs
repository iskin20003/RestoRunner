﻿using Restorunner.Core.DAL.Entities;

namespace Restorunner.Core.DAL.Entities.MenuService;

public class MenuItem : IEntity<long>
{
    public long Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public decimal Price { get; set; }

    //public long CategoryId { get; set; }
    //public Category Category { get; set; }
}