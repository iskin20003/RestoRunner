﻿namespace Restorunner.Core.DAL.Entities.CartService;

public class CartItem : IEntity<long>
{
    public long Id { get; set; }
    public string ClientName { get; set; }
    public string MenuItemName { get; set; }
    public int Quantity { get; set; }
    public decimal Price { get; set; }
}