﻿namespace Restorunner.Core.DAL.Entities.Enums;

public enum OrderType
{
    InRestaurant = 0,
    Delivery = 1,
    TakeAway = 2,
}