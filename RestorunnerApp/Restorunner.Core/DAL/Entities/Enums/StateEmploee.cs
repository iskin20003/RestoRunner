﻿namespace Restorunner.Core.DAL.Entities.Enums;

public enum StateEmployee
{
    DayOff = -1,
    Free = 0,
    Works = 1
}