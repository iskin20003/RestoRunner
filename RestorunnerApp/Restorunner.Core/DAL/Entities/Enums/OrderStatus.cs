﻿namespace Restorunner.Core.DAL.Entities.Enums;

public enum OrderStatus
{
    Canceled = -1,
    Accepted = 0,
    GettingReady = 1,
    Ready = 2,
    Completed = 3
}