﻿namespace Restorunner.Core.DAL.Entities.Enums;

public enum PaymentMethod
{
    CashlessPayment = 0,
    CashPayment = 1
}