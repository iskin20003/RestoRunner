﻿using Restorunner.Core.DAL.Entities.Enums;

namespace Restorunner.Core.DAL.Entities.KitchenService;

public class Cook : IEntity<long>
{
    public long Id { get; set; }

    public string Name { get; set; }

    public StateEmployee State { get; set; }
}