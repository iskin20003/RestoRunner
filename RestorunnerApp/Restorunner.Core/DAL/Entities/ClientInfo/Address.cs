﻿namespace Restorunner.Core.DAL.Entities.ClientInfo;

public class Address
{
    public string City { get; set; }

    public string Street { get; set; }

    public int HouseNumber { get; set; }

    public int? ApartmentNumber { get; set; }
}