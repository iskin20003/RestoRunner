﻿using Restorunner.Core.DAL.Entities;

namespace Restorunner.Core.DAL.Entities.ClientInfo;

public class Client : IEntity<long>
{
    public long Id { get; set; }

    public long PhoneNumber { get; set; }

    public string Name { get; set; }

    public Address? Address { get; set; }
}