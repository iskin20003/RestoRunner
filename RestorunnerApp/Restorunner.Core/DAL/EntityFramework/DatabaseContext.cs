﻿using Microsoft.EntityFrameworkCore;
using Restorunner.Core.DAL.Entities.CartService;
using Restorunner.Core.DAL.Entities.ClientInfo;
using Restorunner.Core.DAL.Entities.KitchenService;
using Restorunner.Core.DAL.Entities.MenuService;
using Restorunner.Core.DAL.Entities.OrderService;

namespace Restorunner.Core.DAL.EntityFramework;

public class DatabaseContext: DbContext
{
    public DbSet<Order> Orders { get; set; }
    public DbSet<OrderItem> OrderItems { get; set; }
    public DbSet<CartItem> CartItems { get; set; }
    public DbSet<MenuItem> MenuItems { get; set; }

    public DbSet<Cook> Cooks { get; set; }
    public DbSet<Waiter> Waiters { get; set; }

    public DbSet<Client> Clients { get; set; }

    public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) { }

    //public DatabaseContext() { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Client>()
            .OwnsOne(x => x.Address);

        modelBuilder.Entity<Order>().Navigation(x => x.OrderItems).AutoInclude();

        modelBuilder.Entity<Order>()
            .Property(x => x.CreateOrder)
            .HasConversion(
                v => v.UtcDateTime,
                v => new DateTimeOffset(v, TimeSpan.Zero));

        base.OnModelCreating(modelBuilder);
    }

    //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    //{
    //    base.OnConfiguring(optionsBuilder);

    //    if (!optionsBuilder.IsConfigured)
    //    {
    //        optionsBuilder.UseNpgsql(
    //            "Host=localhost;Port=5432;Database=Restorunner;Username=postgres;Password=postgres");
    //    }
    //}
}