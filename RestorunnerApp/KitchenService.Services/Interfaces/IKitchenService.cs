﻿using KitchenService.Contract.Contract;

namespace KitchenService.Services.Interfaces;

public interface IKitchenService
{
    Task AddOrderToQueue(long orderId);

    Task StartCooking(long orderId);

    Task FinishCooking(long orderId);

    Task PlaceOrder(long orderId);

    Task StartService();
}