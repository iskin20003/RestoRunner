﻿using KitchenService.Contract.Contract;
using Restorunner.Core.DAL.Entities.Enums;

namespace KitchenService.Services.Interfaces;

public interface IEmployeeService
{
    Task<ICollection<CookDto>> GetAllCooks();

    Task<ICollection<WaiterDto>> GetAllWaiters();

    Task<CookDto?> GetFreeCook();

    Task<WaiterDto?> GetFreeWaiter();

    Task ChangeStatusCook(StateEmployee newState, long cookId);

    Task ChangeStatusWaiters(StateEmployee newState, long waiterId);
}