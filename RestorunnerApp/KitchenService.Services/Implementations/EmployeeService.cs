﻿using AutoMapper;
using KitchenService.Contract.Contract;
using KitchenService.Services.Interfaces;
using Restorunner.Core.DAL.Entities.Enums;
using Restorunner.Core.DAL.Entities.KitchenService;
using Restorunner.Core.DAL.Repositories.Abstractions;
using System.Net;

namespace KitchenService.Services.Implementations;

public class EmployeeService(IRepository<Waiter, long> waiterRepository, IRepository<Cook, long> cookRepository, IMapper mapper) : IEmployeeService
{
    public async Task<ICollection<CookDto>> GetAllCooks()
    {
        var cooks = await cookRepository.GetAll();

        return cooks.Select(mapper.Map<Cook, CookDto>).ToList();
    }

    public async Task<ICollection<WaiterDto>> GetAllWaiters()
    {
        var waiters = await waiterRepository.GetAll();

        return waiters.Select(mapper.Map<Waiter, WaiterDto>).ToList();
    }

    public async Task<CookDto?> GetFreeCook()
    {
        var cooks = await GetAllCooks();
        return cooks.FirstOrDefault(cook => cook.State == StateEmployee.Free);
    }

    public async Task<WaiterDto?> GetFreeWaiter()
    {
        var waiters = await GetAllWaiters();

        return waiters.FirstOrDefault(waiter => waiter.State == StateEmployee.Free);
    }

    public async Task ChangeStatusCook(StateEmployee newState, long cookId)
    {
        var cook = await cookRepository.Get(cookId);
        if (cook != null)
        {
            cook.State = newState;
            await cookRepository.Update(cook);
        }
    }

    public async Task ChangeStatusWaiters(StateEmployee newState, long waiterId)
    {
        var waiter = await waiterRepository.Get(waiterId);
        if (waiter != null)
        {
            waiter.State = newState;
            await waiterRepository.Update(waiter);
        }
    }
}