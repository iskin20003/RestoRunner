﻿using AutoMapper;
using KitchenService.Contract.Contract;
using KitchenService.Services.Interfaces;
using Restorunner.Core.DAL.Entities.Enums;
using Restorunner.Core.DAL.Entities.OrderService;
using Restorunner.Core.DAL.Repositories.Abstractions;
using Restorunner.MessageBroker.Bus;
using Restorunner.MessageBroker.Messages.Implementations;

namespace KitchenService.Services.Implementations;

public class KitchenService : IKitchenService
{
    private readonly IReadRepository<Order, long> _orderRepository;
    private readonly IEmployeeService _employeeService;
    private readonly IMessageBus _messageBus;
    private readonly IMapper _mapper;
    private static readonly Queue<OrderDto> _orders = new();
    private static readonly Queue<OrderDto> _readyOrders = new();
    private readonly ManualResetEvent _cookResetEvent = new(true);
    private readonly AutoResetEvent _waiterResetEvent = new(true);
    private readonly Random _random = new();

    public KitchenService(IReadRepository<Order, long> orderRepository, IEmployeeService employeeService, IMessageBus messageBus, IMapper mapper)
    {
        _orderRepository = orderRepository;
        _employeeService = employeeService;
        _messageBus = messageBus;
        _mapper = mapper;
    }

    private async Task ProcessWaiters()
    {
        while (true)
        {
            if (_readyOrders.TryDequeue(out var order))
            {
                _waiterResetEvent.WaitOne();

                var freeWaiter = await _employeeService.GetFreeWaiter();
                while (freeWaiter == null)
                {
                    freeWaiter = await _employeeService.GetFreeWaiter();
                    await Task.Delay(TimeSpan.FromSeconds(1));
                }

                await _employeeService.ChangeStatusWaiters(StateEmployee.Works, freeWaiter.Id);

                _waiterResetEvent.Set();

                await PlaceOrder(order.Id);

                await _employeeService.ChangeStatusWaiters(StateEmployee.Free, freeWaiter.Id);
            }
        }
    }

    private async Task ProcessCooking()
    {
        while (true)
        {
            if (_orders.TryDequeue(out var order))
            {
                _cookResetEvent.WaitOne();
                _cookResetEvent.Reset();
                
                var freeCook = await _employeeService.GetFreeCook();
                while (freeCook == null)
                {
                    freeCook = await _employeeService.GetFreeCook();
                    await Task.Delay(TimeSpan.FromSeconds(1));
                }

                await _employeeService.ChangeStatusCook(StateEmployee.Works, freeCook.Id);

                _cookResetEvent.Set();

                await StartCooking(order.Id);

                var timeCooking = order.OrderItems.Count * _random.Next(100, 200);
                await Task.Delay(TimeSpan.FromSeconds(timeCooking));

                await FinishCooking(order.Id);

                _readyOrders.Enqueue(order);

                await _employeeService.ChangeStatusCook(StateEmployee.Free, freeCook.Id);
            }
        }
    }

    public async Task AddOrderToQueue(long orderId)
    {
        var orderEntity = await _orderRepository.Get(orderId) ??
                          throw new ArgumentOutOfRangeException($"Не найден заказа с таким ид {orderId}");

        var order = _mapper.Map<Order, OrderDto>(orderEntity);

        _orders.Enqueue(order);
    }

    public async Task StartCooking(long orderId)
    {
        _messageBus.Publish(new ChangeStatusOrderMessage(orderId, OrderStatus.Ready));
        await Task.Delay(1000); // Имитация приготовления ингредиентов.
    }

    public async Task FinishCooking(long orderId)
    {
        _messageBus.Publish(new ChangeStatusOrderMessage(orderId, OrderStatus.Completed));
        await Task.Delay(1000); // Имитация завершения работы.
    }

    public async Task PlaceOrder(long orderId)
    {
        await Task.Delay(TimeSpan.FromSeconds(10)); // имитация выноса заказа.
        _messageBus.Publish(new ChangeStatusOrderMessage(orderId, OrderStatus.Accepted));
    }

    public Task StartService()
    {
        Task.Run(ProcessCooking);
        Task.Run(ProcessWaiters);

        return Task.CompletedTask;
    }
}