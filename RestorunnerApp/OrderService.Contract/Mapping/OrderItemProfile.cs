﻿using AutoMapper;
using OrderService.Contract.Contract;
using Restorunner.Core.DAL.Entities.OrderService;

namespace OrderService.Contract.Mapping;

public class OrderItemProfile : Profile
{
    public OrderItemProfile()
    {
        CreateMap<OrderItem, OrderItemDto>();

        CreateMap<OrderItemDto, OrderItem>().ForMember(x => x.Order, src => src.Ignore());
    }
}