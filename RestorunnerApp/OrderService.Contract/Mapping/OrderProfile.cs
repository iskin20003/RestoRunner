﻿using AutoMapper;
using OrderService.Contract.Contract;
using Restorunner.Core.DAL.Entities.OrderService;

namespace OrderService.Contract.Mapping;

public class OrderProfile : Profile
{
    public OrderProfile()
    {
        CreateMap<Order, OrderDto>().ReverseMap();
    }
}