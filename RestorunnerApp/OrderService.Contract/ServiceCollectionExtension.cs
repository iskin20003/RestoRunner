﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using OrderService.Contract.Mapping;

namespace OrderService.Contract;

public static class ServiceCollectionExtension
{
    public static IServiceCollection AddMapping(this IServiceCollection services)
    {
        services.AddSingleton<IMapper>(new Mapper(CreateMapping()));

        return services;
    }

    private static IConfigurationProvider CreateMapping()
    {
        var config = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<OrderProfile>();
            cfg.AddProfile<OrderItemProfile>();
        });

        config.AssertConfigurationIsValid();

        return config;
    }
}