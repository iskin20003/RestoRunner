﻿using Restorunner.Core.DAL.Entities.OrderService;

namespace OrderService.Contract.Contract;

public class OrderItemDto
{
    public long Id { get; set; }

    public long OrderId { get; set; }

    public string MenuItemName { get; set; }

    public int Quantity { get; set; }

    public decimal TotalPrice { get; set; }
}