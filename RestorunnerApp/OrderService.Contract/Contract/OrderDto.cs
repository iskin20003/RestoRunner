﻿using Restorunner.Core.DAL.Entities.Enums;
using Restorunner.Core.DAL.Entities.OrderService;

namespace OrderService.Contract.Contract;

public class OrderDto
{
    public long Id { get; set; }

    public string ClientName { get; set; }

    public ICollection<OrderItemDto> OrderItems { get; set; }

    public DateTimeOffset CreateOrder { get; set; }

    public OrderStatus Status { get; set; }

    public OrderType OrderType { get; set; }

    public PaymentMethod PaymentMethod { get; set; }

    public decimal TotalPrice => OrderItems.Sum(orderItem => orderItem.TotalPrice);
}