﻿using Restorunner.MessageBroker.Bus;
using Restorunner.MessageBroker.Subscribtions;
using System.Net.Sockets;
using System.Reflection;
using System.Text.Json;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Polly;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;
using Restorunner.MessageBroker.RabbitMq.Connection;
using Restorunner.MessageBroker.Messages.Abstractions;

namespace Restorunner.MessageBroker.RabbitMq.Bus;

public class RabbitMqMessageBus : IMessageBus
{
    private readonly IPersistentConnection _connection;
    private readonly IMessageBusSubscriptionManager _manager;
    private readonly IServiceProvider _services;
    private readonly string _exchangeName;
    private readonly string _queueName;
    private readonly int _publishRetryCount = 5;
    private readonly TimeSpan _subscribeRetryTime = TimeSpan.FromSeconds(5);

    private IModel _consumerChannel;

    public RabbitMqMessageBus(IPersistentConnection connection, IMessageBusSubscriptionManager manager,
        IServiceProvider services, string exchangeName, string queueName)
    {
        _connection = connection;
        _manager = manager;
        _services = services;
        _exchangeName = exchangeName;
        _queueName = queueName;

        ConfigureMessageBroker();
    }

    #region Methods

    private void ConfigureMessageBroker()
    {
        _consumerChannel = CreateConsumerChannel();
        _manager.OnMessageRemovedHandler += Manager_OnMessageRemovedHandler;
        _connection.OnReconnectedAfterConnectionFailure += Connection_OnReconnectedAfterConnectionFailure;
    }

    private IModel CreateConsumerChannel()
    {
        if (!_connection.IsConnected)
            _connection.TryConnect();

        var channel = _connection.CreateClient();

        channel.ExchangeDeclare(_exchangeName, "direct");
        channel.QueueDeclare(_queueName, true, false, false, null);
        channel.CallbackException += (sender, e) =>
        {
            DoCreateConsumerChannel();
        };

        return channel;
    }

    private void DoCreateConsumerChannel()
    {
        //_consumerChannel?.Dispose();

        _consumerChannel = CreateConsumerChannel();
        StartBasicConsume();
    }

    private void StartBasicConsume()
    {
        if (_consumerChannel == null)
            return;

        var consumer = new AsyncEventingBasicConsumer(_consumerChannel);
        consumer.Received += Consumer_Received;

        _consumerChannel.BasicConsume(consumer, _queueName);
    }

    private async Task Consumer_Received(object sender, BasicDeliverEventArgs eventArgs)
    {
        var messageName = eventArgs.RoutingKey;
        var message = Encoding.UTF8.GetString(eventArgs.Body.Span);

        var isAcknowledged = false;

        try
        {
            await ProcessMessage(messageName, message);
            _consumerChannel.BasicAck(eventArgs.DeliveryTag, false);

            isAcknowledged = true;
        }
        finally
        {
            if (!isAcknowledged)
                await TryEnqueueMessageAgainAsync(eventArgs);
        }
    }

    private async Task TryEnqueueMessageAgainAsync(BasicDeliverEventArgs eventArgs)
    {
        try
        {
            await Task.Delay(_subscribeRetryTime);
            _consumerChannel.BasicNack(eventArgs.DeliveryTag, false, true);
        }
        catch (Exception) { }
    }

    private async Task ProcessMessage(string messageName, string message)
    {
        if (!_manager.HasSubscriptionForMessage(messageName))
            return;

        var subscriptions = _manager.GetSubscriptionsForMessage(messageName);
        foreach (var subscription in subscriptions)
        {
            using var scope = _services.CreateScope();
            var handler = scope.ServiceProvider.GetService(subscription.HandlerType);
            //object? handler = null;

            //    handler = _services.GetService(subscription.HandlerType);
            if (handler == null)
                continue;

            var messageType = _manager.GetMessageTypeByName(messageName);
            if (messageType == null)
                continue;

            try
            {
                var messageContnet = JsonSerializer.Deserialize(message, messageType);
                if (messageContnet == null)
                    continue;
                var messageHandlerType = typeof(IMessageHandler<>).MakeGenericType(messageType);
                await Task.Yield();
                var method = messageHandlerType.GetMethod(nameof(IMessageHandler<Message>.HandleAsync));
                if (method == null)
                    continue;
                await (Task)method.Invoke(handler, [messageContnet])!;
            }
            catch { }
        }
    }

    private void Manager_OnMessageRemovedHandler(object? sender, string e)
    {
        if (!_connection.IsConnected)
            _connection.TryConnect();

        using var channel = _connection.CreateClient();

        channel.QueueUnbind(_queueName, _exchangeName, e, null);

        if (_manager.IsEmpty)
            _consumerChannel.Close();
    }

    private void Connection_OnReconnectedAfterConnectionFailure(object? sender, EventArgs e)
    {
        DoCreateConsumerChannel();
        RecreateSubscription();
    }

    private void RecreateSubscription()
    {
        var subscriptions = _manager.GetAllSubscriptions();
        _manager.Clear();

        var eventBusType = GetType();
        MethodInfo genericSubscribe;

        foreach (var entry in subscriptions)
        {
            foreach (var subscription in entry.Value)
            {
                genericSubscribe = eventBusType.GetMethod(nameof(Subscribe))!.MakeGenericMethod(subscription.MessageType, subscription.HandlerType);
                genericSubscribe.Invoke(this, null);
            }
        }
    }

    private void AddQueueBindForMessageSubscription(string messageName)
    {
        if (_manager.HasSubscriptionForMessage(messageName))
            return;

        if (!_connection.IsConnected)
            _connection.TryConnect();

        //using var channel = _connection.CreateClient();

        _consumerChannel.QueueBind(_queueName, _exchangeName, messageName, null);
        _consumerChannel.QueueDeclare(_queueName, true, false, false);
    }

    #endregion

    #region Override methods

    public void Publish<TMessage>(TMessage message) where TMessage : Message
    {
        if (!_connection.IsConnected)
            _connection.TryConnect();

        var policy = Policy.Handle<BrokerUnreachableException>()
            .Or<SocketException>()
            .WaitAndRetry(_publishRetryCount, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));

        var messageName = message.GetType().Name;

        using (var channel = _connection.CreateClient())
        {
            channel.ExchangeDeclare(_exchangeName, ExchangeType.Direct);
            //channel.QueueDeclare(_queueName, false, false, false);
            //channel.QueueBind(_queueName, _exchangeName, messageName, null);

            var messageBody = JsonSerializer.Serialize(message);
            var messageBytes = Encoding.UTF8.GetBytes(messageBody);

            policy.Execute(() =>
            {
                var properties = channel.CreateBasicProperties();
                properties.DeliveryMode = (byte)2;

                channel.BasicPublish(_exchangeName, messageName, properties, messageBytes);
            });
        }
    }

    public void Subscribe<TMessage, TMessageHandler>()
        where TMessage : Message
        where TMessageHandler : IMessageHandler<TMessage>
    {
        var messageName = _manager.GetMessageIndentifier<TMessage>();

        AddQueueBindForMessageSubscription(messageName);

        _manager.AddSubscription<TMessage, TMessageHandler>();
        StartBasicConsume();
    }

    public void Unsubscribe<TMessage, TMessageHandler>()
        where TMessage : Message
        where TMessageHandler : IMessageHandler<TMessage>
    {
        _manager.RemoveSubscription<TMessage, TMessageHandler>();
    }

    #endregion
}