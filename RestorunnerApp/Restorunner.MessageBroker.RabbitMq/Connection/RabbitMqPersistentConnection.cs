﻿using Polly;
using RabbitMQ.Client.Exceptions;
using RabbitMQ.Client;
using System.Net.Sockets;

namespace Restorunner.MessageBroker.RabbitMq.Connection;

public class RabbitMqPersistentConnection : IPersistentConnection, IDisposable
{
    public event EventHandler OnReconnectedAfterConnectionFailure;

    #region Fields

    private readonly IConnectionFactory _connectionFactory;

    private readonly TimeSpan _timeoutBeforeReconnecting;

    private IConnection _connection;
    private bool _disposed;

    private readonly object _locker = new();

    private bool _connectionFailed;

    #endregion

    #region Properties

    public bool IsConnected => (_connection != null && _connection.IsOpen && !_disposed);

    #endregion

    #region Constructor

    public RabbitMqPersistentConnection(IConnectionFactory connectionFactory, int timeoutBeforeReconnecting = 10)
    {
        _connectionFactory = connectionFactory;
        _timeoutBeforeReconnecting = TimeSpan.FromSeconds(timeoutBeforeReconnecting);
    }

    #endregion


    #region Override methods

    public IModel CreateClient()
    {
        if (!IsConnected)
            throw new InvalidOperationException();

        return _connection.CreateModel();
    }

    public bool TryConnect()
    {
        lock (_locker)
        {
            var policy = Policy
                .Handle<SocketException>()
                .Or<BrokerUnreachableException>()
                .WaitAndRetryForever((duration) => _timeoutBeforeReconnecting, (ex, time) => { });

            policy.Execute(() =>
            {
                _connection = _connectionFactory.CreateConnection();
            });

            if (!IsConnected)
            {
                _connectionFailed = true;
                return false;
            }

            _connection.ConnectionShutdown += ConnectionShutdown;
            _connection.CallbackException += CallbackException;
            _connection.ConnectionBlocked += ConnectionBlocked;
            _connection.ConnectionUnblocked += ConnectionUnblocked;

            if (_connectionFailed)
            {
                OnReconnectedAfterConnectionFailure?.Invoke(this, EventArgs.Empty);
                _connectionFailed = false;
            }

            return true;
        }
    }

    public void Dispose()
    {
        _disposed = true;
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    #endregion

    #region EventHandlers

    private void ConnectionUnblocked(object? sender, EventArgs e)
    {
        _connectionFailed = true;

        TryConnectIfNotDisposed();
    }

    private void ConnectionBlocked(object? sender, global::RabbitMQ.Client.Events.ConnectionBlockedEventArgs e)
    {
        _connectionFailed = true;

        TryConnectIfNotDisposed();
    }

    private void CallbackException(object? sender, global::RabbitMQ.Client.Events.CallbackExceptionEventArgs e)
    {
        _connectionFailed = true;

        TryConnectIfNotDisposed();
    }

    private void ConnectionShutdown(object? sender, ShutdownEventArgs e)
    {
        _connectionFailed = true;

        TryConnectIfNotDisposed();
    }

    #endregion

    protected virtual void Dispose(bool isDisposing)
    {
        if (isDisposing)
            _connection.Dispose();
    }

    private void TryConnectIfNotDisposed()
    {
        if (_disposed)
            return;

        TryConnect();
    }
}