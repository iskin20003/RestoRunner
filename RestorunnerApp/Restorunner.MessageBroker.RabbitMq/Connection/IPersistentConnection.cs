﻿using RabbitMQ.Client;

namespace Restorunner.MessageBroker.RabbitMq.Connection;

public interface IPersistentConnection
{
    event EventHandler OnReconnectedAfterConnectionFailure;

    bool IsConnected { get; }

    bool TryConnect();

    IModel CreateClient();
}