﻿using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Client;
using Restorunner.MessageBroker.Bus;
using Restorunner.MessageBroker.RabbitMq.Bus;
using Restorunner.MessageBroker.RabbitMq.Connection;
using Restorunner.MessageBroker.Subscribtions;

namespace Restorunner.MessageBroker.RabbitMq;

public static class ServiceCollectionExtension
{
    public static IServiceCollection AddRabbitMqMessageBus(this IServiceCollection services,
        string host, string exchangeName, string queueName, int timeoutBeforeReconnected = 10)
    {
        services.AddSingleton<IMessageBusSubscriptionManager, InMemoryMessageBusSubscriptionManager>()
            .AddSingleton<IPersistentConnection, RabbitMqPersistentConnection>(factory =>
            {
                var connectionFactory = new ConnectionFactory()
                {
                    HostName = "localhost",
                    DispatchConsumersAsync = true
                };

                return new RabbitMqPersistentConnection(connectionFactory, timeoutBeforeReconnected);
            })
            .AddSingleton<IMessageBus, RabbitMqMessageBus>(factory =>
            {
                var persistentConnection = factory.GetService<IPersistentConnection>();
                var subscribeManager = factory.GetService<IMessageBusSubscriptionManager>();

                return new RabbitMqMessageBus(persistentConnection, subscribeManager, factory, exchangeName, queueName);
            });

        return services;
    }
}