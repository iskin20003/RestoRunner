﻿using System.Reflection;
using MenuService.Infrastructure.Repositories.Implementation;
using MenuService.Services.Implementation;
using MenuService.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Restorunner.Core.DAL.Entities.MenuService;
using Restorunner.Core.DAL.EntityFramework;
using Restorunner.Core.DAL.Repositories.Abstractions;
using Restorunner.MessageBroker.RabbitMq;
using MenuService.Contract;

namespace MenuService.WebApi;

public class Startup(IConfiguration configuration)
{
    public void ConfigureServices(IServiceCollection services)
    {
        services
            .AddDbContext<DatabaseContext>(options =>
            {
                options.UseNpgsql(configuration.GetConnectionString("DefaultConnection"));
            })
            .AddScoped<IRepository<MenuItem, long>, MenuItemRepository>()
            .AddScoped<IMenuItemService, MenuItemService>()
            .AddControllers();

        services.AddMapping();

        services.AddSwaggerGen(options =>
        {
            options.SwaggerDoc("v1",
                new OpenApiInfo
                {
                    Title = "Menu Service API",
                    Version = "v1"
                });

            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
            options.IncludeXmlComments(xmlPath);
        });

        AddMessageBroker(services);
    }

    private void AddMessageBroker(IServiceCollection services)
    {
        var sectionRabbitMq = configuration.GetSection("RabbitMq");

        var host = sectionRabbitMq.GetSection("Host").Value;
        var exchange = sectionRabbitMq.GetSection("Exchange").Value;
        var queue = sectionRabbitMq.GetSection("Queue").Value;

        services.AddRabbitMqMessageBus(host, exchange, queue);
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }

        app.UseRouting();
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });

        app.UseSwagger();
        app.UseSwaggerUI(c =>
        {
            c.SwaggerEndpoint("/swagger/v1/swagger.json", "MenuService API v1");
        });
    }
}