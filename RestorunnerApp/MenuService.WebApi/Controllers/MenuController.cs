﻿using MenuService.Contract.Contract;
using MenuService.Services.Implementation;
using MenuService.Services.Interfaces;
using MenuService.WebApi.Models;
using Microsoft.AspNetCore.Mvc;
using Restorunner.Core.DAL.Entities.MenuService;
using Restorunner.MessageBroker.Bus;
using Restorunner.MessageBroker.Messages.Implementations;

namespace MenuService.WebApi.Controllers;

[ApiController]
[Route("api/[controller]")]
public class MenuController(IMenuItemService menuService, IMessageBus messageBus) : ControllerBase
{
    /// <summary>
    /// Получить список всего меню.
    /// </summary>
    /// <returns>Элементы меню</returns>
    [HttpGet]
    public async Task<ActionResult<IEnumerable<MenuItemDto>>> GetAllMenuItems()
    {
        var menuItems = await menuService.GetMenuItemsAsync();

        return Ok(menuItems);
    }

    /// <summary>
    /// Получить элемент меню по номеру.
    /// </summary>
    /// <param name="id">Идентификационный номер.</param>
    /// <returns>Элемент списка меню.</returns>
    [HttpGet("{id}")]
    public async Task<ActionResult<MenuItemDto>> GetMenuItem(long id)
    {
        var menuItem = await menuService.GetMenuItemByIdAsync(id);

        if (menuItem == null)
        {
            return NotFound();
        }

        return Ok(menuItem);
    }

    /// <summary>
    /// Создать новый элемент меню.
    /// </summary>
    /// <param name="menuItem">Новый элемент.</param>
    /// <returns>Созданный элемент.</returns>
    [HttpPost]
    public async Task<ActionResult<MenuItemDto>> CreateMenuItem(MenuItemDto menuItem)
    {
        await menuService.AddMenuItemAsync(menuItem);

        return CreatedAtAction(nameof(GetMenuItem), new { id = menuItem.Id }, menuItem);
    }

    /// <summary>
    /// Изменить элемент меню.
    /// </summary>
    /// <param name="id">Ид элемента меню.</param>
    /// <param name="menuItem">Измененный элемент меню.</param>
    /// <returns></returns>
    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateMenuItem(int id, MenuItemDto menuItem)
    {
        if (id != menuItem.Id)
        {
            return BadRequest();
        }

        await menuService.UpdateMenuItemAsync(menuItem);
        return NoContent();
    }

    /// <summary>
    /// Удалить элемент меню.
    /// </summary>
    /// <param name="id">Ид элемента меню.</param>
    /// <returns></returns>
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteMenuItem(long id)
    {
        await menuService.DeleteMenuItemAsync(id);
        return NoContent();
    }

    /// <summary>
    /// Добавить в корзину.
    /// </summary>
    /// <param name="menuItemId">Индекс элемента меню из списка.</param>
    /// <param name="quantity">Количество.</param>
    /// <returns></returns>
    [HttpPost("Add-To-Cart")]
    public async Task<ActionResult> AddMenuItemToCart([FromBody] AddToCartRequest request)
    {
        var menuItem = await menuService.GetMenuItemByIdAsync(request.MenuItemId);
        if (menuItem == null)
        {
            return BadRequest();
        }

        var message = new AddMenuItemToCartMessage(menuItem.Name, request.Quantity, request.ClientName, menuItem.Price);

        messageBus.Publish(message);

        return Ok("Товар добавлен");
    }
}