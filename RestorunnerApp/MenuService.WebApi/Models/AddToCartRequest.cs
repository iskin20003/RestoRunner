﻿namespace MenuService.WebApi.Models
{
    public class AddToCartRequest
    {
        public long MenuItemId { get; set; }
        public int Quantity { get; set; }
        public string ClientName { get; set; }
    }
}
