﻿using OrderService.Contract.Contract;
using Restorunner.Core.DAL.Entities.Enums;

namespace OrderService.Services.Interfaces;

public interface IOrderService
{
    Task<ICollection<OrderDto>> GetAllOrders(string clientName);

    Task<OrderDto> CreateOrder(OrderDto orderDto);

    Task<OrderDto> GetCurrentOrder(long orderId);

    Task TransferToCooking(long orderId);

    Task RefreshOrderStatus(long orderId, OrderStatus orderStatus);
}