﻿using AutoMapper;
using OrderService.Contract.Contract;
using OrderService.Services.Interfaces;
using Restorunner.Core.DAL.Entities.Enums;
using Restorunner.Core.DAL.Entities.OrderService;
using Restorunner.Core.DAL.Repositories.Abstractions;
using Restorunner.MessageBroker.Bus;
using Restorunner.MessageBroker.Messages.Implementations;

namespace OrderService.Services.Implementations;

public class OrderService(IRepository<Order, long> orderRepository, IMapper mapper, IMessageBus messageBus) : IOrderService
{
    public async Task<ICollection<OrderDto>> GetAllOrders(string clientName)
    {
        var orders = await orderRepository.GetAll();

        var ordersDto = orders.Select(mapper.Map<Order, OrderDto>);

        return ordersDto.ToList();
    }

    public async Task<OrderDto> CreateOrder(OrderDto orderDto)
    {
        var order = mapper.Map<OrderDto, Order>(orderDto);
        var id = await orderRepository.Add(order);

        orderDto.Id = id;

        for (int i = 0; i < orderDto.OrderItems.Count; i++)
        {
            orderDto.OrderItems.ElementAt(i).Id = order.OrderItems.ElementAt(i).Id;
            orderDto.OrderItems.ElementAt(i).OrderId = id;
        }

        return orderDto;
    }

    public async Task<OrderDto> GetCurrentOrder(long orderId)
    {
        var order = await orderRepository.Get(orderId) ?? throw new Exception($"Не найден заказ с таким ид = {orderId}");
        
        return mapper.Map<Order, OrderDto>(order);
    }

    public Task TransferToCooking(long orderId)
    {
        messageBus.Publish(new SendOrderForCookingMessage(orderId));

        return Task.CompletedTask;
    }

    public async Task RefreshOrderStatus(long orderId, OrderStatus orderStatus)
    {
        var order = await orderRepository.Get(orderId) ?? throw new Exception($"Не найден заказ с таким ид = {orderId}");
        
        order.Status = orderStatus;

        await orderRepository.Update(order);
    }
}