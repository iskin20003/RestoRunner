﻿using AutoMapper;
using KitchenService.Contract.Contract;
using Restorunner.Core.DAL.Entities.KitchenService;

namespace KitchenService.Contract.Mapping;

public class CookProfile : Profile
{
    public CookProfile()
    {
        CreateMap<Cook, CookDto>().ReverseMap();
    }
}