﻿using System.Diagnostics.Tracing;
using AutoMapper;
using KitchenService.Contract.Contract;
using Restorunner.Core.DAL.Entities.OrderService;

namespace KitchenService.Contract.Mapping;

public class OrderProfile : Profile
{
    public OrderProfile()
    {
        CreateMap<OrderDto, Order>()
            .ForMember(x => x.CreateOrder, src => src.Ignore())
            .ForMember(x => x.Status, src => src.Ignore())
            .ForMember(x => x.ClientName, src => src.Ignore())
            .ForMember(x => x.OrderType, src => src.Ignore())
            .ForMember(x => x.PaymentMethod, src => src.Ignore())
            .ForMember(x => x.TotalPrice, src => src.Ignore());
    }
}