﻿using AutoMapper;
using KitchenService.Contract.Contract;
using Restorunner.Core.DAL.Entities.OrderService;

namespace KitchenService.Contract.Mapping;

public class OrderItemProfile : Profile
{
    public OrderItemProfile()
    {
        CreateMap<OrderItemDto, OrderItem>()
            .ForMember(x => x.Id, src => src.Ignore())
            .ForMember(x => x.Order, src => src.Ignore())
            .ForMember(x => x.TotalPrice, src => src.Ignore())
            .ForMember(x => x.OrderId, src => src.Ignore())
            .ForMember(x => x.MenuItemName, src => src.MapFrom(x => x.Name));
    }
}