﻿using AutoMapper;
using KitchenService.Contract.Contract;
using Restorunner.Core.DAL.Entities.KitchenService;

namespace KitchenService.Contract.Mapping;

public class WaiterProfile : Profile
{
    public WaiterProfile()
    {
        CreateMap<Waiter, WaiterDto>().ReverseMap();
    }
}