﻿using Restorunner.Core.DAL.Entities.Enums;

namespace KitchenService.Contract.Contract;

public class CookDto
{
    public long Id { get; set; }

    public string Name { get; set; }

    public StateEmployee State { get; set; }
}