﻿namespace KitchenService.Contract.Contract;

public class OrderItemDto
{
    public string Name { get; set; }

    public int Quantity { get; set; }
}