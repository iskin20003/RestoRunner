﻿namespace KitchenService.Contract.Contract;

public class OrderDto
{
    public long Id { get; set; }

    public ICollection<OrderItemDto> OrderItems { get; set; }
}