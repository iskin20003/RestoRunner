﻿using AutoMapper;
using KitchenService.Contract.Mapping;
using Microsoft.Extensions.DependencyInjection;

namespace KitchenService.Contract;

public static class ServiceCollectionExtension
{
    public static IServiceCollection AddMapping(this IServiceCollection services)
    {
        services.AddSingleton<IMapper>(new Mapper(ConfigureMappings()));

        return services;
    }

    private static IConfigurationProvider ConfigureMappings()
    {
        var config = new MapperConfiguration(config =>
        {
            config.AddProfile<OrderProfile>();
            config.AddProfile<OrderItemProfile>();
            config.AddProfile<WaiterProfile>();
            config.AddProfile<CookProfile>();
        });

        config.AssertConfigurationIsValid();
        return config;
    }
}