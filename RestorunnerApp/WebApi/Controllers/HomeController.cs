﻿using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

public class HomeController : Controller
{
    // GET
    public IActionResult Index()
    {
        return View();
    }
}