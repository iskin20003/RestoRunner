﻿using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using OrderService.Contract;
using OrderService.Infrastructure.Repositories.Implementations;
using OrderService.Services.Interfaces;
using OrderService.WebApi.MessageHandlers;
using Restorunner.Core.DAL.Entities.OrderService;
using Restorunner.Core.DAL.EntityFramework;
using Restorunner.Core.DAL.Repositories.Abstractions;
using Restorunner.MessageBroker.Bus;
using Restorunner.MessageBroker.Messages.Implementations;
using Restorunner.MessageBroker.RabbitMq;
using System.Reflection;

namespace OrderService.WebApi;

public class Startup(IConfiguration configuration)
{
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddDbContext<DatabaseContext>(options =>
        {
            options.UseNpgsql(configuration.GetConnectionString("DefaultConnection"));
        });

        services.AddScoped<IRepository<Order, long>, OrderRepository>()
            .AddScoped<IRepository<OrderItem, long>, OrderItemRepository>()
            .AddScoped<IOrderService, Services.Implementations.OrderService>()
            .AddMapping()
            .AddControllers();

        AddMessageBroker(services);

        services.AddSwaggerGen(options =>
        {
            options.SwaggerDoc("v1",
                new OpenApiInfo
                {
                    Title = "Order Service API",
                    Version = "v1"
                });

            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
            options.IncludeXmlComments(xmlPath);
        });
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }

        app.UseRouting();
        app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

        app.UseSwagger();
        app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "Order API V1"); });

        var messageBus = app.ApplicationServices.GetService<IMessageBus>();
        messageBus!.Subscribe<CreateOrderMessage, CreateOrderMessageHandler>();
        messageBus.Subscribe<ChangeStatusOrderMessage, ChangeStatusOrderMessageHandler>();
    }


    private void AddMessageBroker(IServiceCollection services)
    {
        var sectionRabbitMq = configuration.GetSection("RabbitMq");

        var host = sectionRabbitMq.GetSection("Host").Value;
        var exchange = sectionRabbitMq.GetSection("Exchange").Value;
        var queue = sectionRabbitMq.GetSection("Queue").Value;

        services.AddRabbitMqMessageBus(host, exchange, queue)
            .AddTransient<CreateOrderMessageHandler>()
            .AddTransient<ChangeStatusOrderMessageHandler>();
    }
}