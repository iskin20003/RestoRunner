﻿using OrderService.Services.Interfaces;
using Restorunner.MessageBroker.Messages.Abstractions;
using Restorunner.MessageBroker.Messages.Implementations;

namespace OrderService.WebApi.MessageHandlers;

public class ChangeStatusOrderMessageHandler(IOrderService orderService) : IMessageHandler<ChangeStatusOrderMessage>
{
    public async Task HandleAsync(ChangeStatusOrderMessage message)
    {
        await orderService.RefreshOrderStatus(message.OrderId, message.NewOrderStatus);
    }
}