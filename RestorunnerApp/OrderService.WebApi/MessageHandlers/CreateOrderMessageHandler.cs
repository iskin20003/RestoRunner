﻿using OrderService.Contract.Contract;
using OrderService.Services.Interfaces;
using Restorunner.Core.DAL.Entities.Enums;
using Restorunner.MessageBroker.Messages.Abstractions;
using Restorunner.MessageBroker.Messages.Implementations;

namespace OrderService.WebApi.MessageHandlers;

public class CreateOrderMessageHandler(IOrderService service) : IMessageHandler<CreateOrderMessage>
{
    public async Task HandleAsync(CreateOrderMessage message)
    {
        var oderItems = message.CartItems.Select(x => new OrderItemDto
        {
            Quantity = x.Quantity,
            MenuItemName = x.MenuItemName,
            TotalPrice = x.Price
        }).ToList();

        var order = await service.CreateOrder(new OrderDto
        {
            ClientName = message.ClientName,
            CreateOrder = DateTimeOffset.Now.ToUniversalTime(),
            OrderType = OrderType.InRestaurant,
            PaymentMethod = PaymentMethod.CashlessPayment,
            Status = OrderStatus.GettingReady,
            OrderItems = oderItems
        });

        await service.TransferToCooking(order.Id);
    }
}