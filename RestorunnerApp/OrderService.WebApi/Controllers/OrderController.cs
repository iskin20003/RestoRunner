﻿using Microsoft.AspNetCore.Mvc;
using OrderService.Contract.Contract;
using OrderService.Services.Interfaces;

namespace OrderService.WebApi.Controllers;

[ApiController]
[Route("api/[controller]")]
public class OrderController(IOrderService orderService) : ControllerBase
{
    [HttpGet("get-all")]
    public async Task<ActionResult<ICollection<OrderDto>>> GetAllOrdersFromClient(string clientName)
    {
        var orders = await orderService.GetAllOrders(clientName);

        return Ok(orders);
    }

    [HttpGet]
    public async Task<ActionResult<OrderDto>> GetCurrentOrder(long orderId)
    {
        try
        {
            return await orderService.GetCurrentOrder(orderId);
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }
}