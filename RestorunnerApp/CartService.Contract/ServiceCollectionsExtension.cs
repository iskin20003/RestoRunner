﻿using AutoMapper;
using CartService.Contract.Mappings;
using Microsoft.Extensions.DependencyInjection;

namespace CartService.Contract;

public static class ServiceCollectionsExtension
{
    public static IServiceCollection AddMapping(this IServiceCollection services)
    {
        services.AddSingleton<IMapper>(new Mapper(ConfigureMappings()));

        return services;
    }

    private static IConfigurationProvider ConfigureMappings()
    {
        var config = new MapperConfiguration(config =>
        {
            config.AddProfile<CartItemMappingProfile>();
        });

        config.AssertConfigurationIsValid();
        return config;
    }
}