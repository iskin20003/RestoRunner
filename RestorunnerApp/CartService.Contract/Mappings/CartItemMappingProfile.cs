﻿using AutoMapper;
using CartService.Contract.Contract;
using Restorunner.Core.DAL.Entities.CartService;

namespace CartService.Contract.Mappings;

public class CartItemMappingProfile : Profile
{
    public CartItemMappingProfile()
    {
        CreateMap<CartItem, CartItemDto>().ReverseMap();
    }
}