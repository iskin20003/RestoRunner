﻿using Microsoft.EntityFrameworkCore;
using Restorunner.Core.DAL.Entities.MenuService;
using Restorunner.Core.DAL.EntityFramework;
using Restorunner.Core.DAL.Repositories.Abstractions;

namespace MenuService.Infrastructure.Repositories.Implementation;

public class MenuItemRepository(DatabaseContext dbContext) : Repository<MenuItem, long>(dbContext)
{
    
}