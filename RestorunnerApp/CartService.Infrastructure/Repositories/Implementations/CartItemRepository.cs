﻿using Restorunner.Core.DAL.Entities.CartService;
using Restorunner.Core.DAL.EntityFramework;
using Restorunner.Core.DAL.Repositories.Abstractions;

namespace CartService.Infrastructure.Repositories.Implementations;

public class CartItemRepository(DatabaseContext dbContext) : Repository<CartItem, long>(dbContext)
{
    
}