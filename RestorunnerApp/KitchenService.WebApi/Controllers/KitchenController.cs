﻿using KitchenService.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KitchenService.WebApi.Controllers;

[Route("api/[controller]")]
[ApiController]
public class KitchenController(IKitchenService kitchenService) : ControllerBase
{
    [HttpPost]
    public IActionResult StartKitchen()
    {
        kitchenService.StartService();

        return Ok("Кухня запущена");
    }
}