﻿using KitchenService.Services.Interfaces;
using Restorunner.MessageBroker.Messages.Abstractions;
using Restorunner.MessageBroker.Messages.Implementations;

namespace KitchenService.WebApi.MessageHandlers;

public class SendOrderForCookingMessageHandler(IKitchenService kitchenService) : IMessageHandler<SendOrderForCookingMessage>
{
    public Task HandleAsync(SendOrderForCookingMessage message)
    {
        kitchenService.AddOrderToQueue(message.OrderId);

        return Task.CompletedTask;
    }
}