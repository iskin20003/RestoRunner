﻿using KitchenService.Contract;
using KitchenService.Infrastructure.Repositories.Implementations;
using KitchenService.Services.Implementations;
using KitchenService.Services.Interfaces;
using KitchenService.WebApi.MessageHandlers;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Restorunner.Core.DAL.Entities.KitchenService;
using Restorunner.Core.DAL.Entities.OrderService;
using Restorunner.Core.DAL.EntityFramework;
using Restorunner.Core.DAL.Repositories.Abstractions;
using Restorunner.MessageBroker.Bus;
using Restorunner.MessageBroker.Messages.Implementations;
using Restorunner.MessageBroker.RabbitMq;
using System.Reflection;

namespace KitchenService.WebApi;

public class Startup(IConfiguration configuration)
{
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddDbContext<DatabaseContext>(options =>
        {
            options.UseNpgsql(configuration.GetConnectionString("DefaultConnection"));
        });

        services.AddTransient<IRepository<Cook, long>, CookRepository>()
            .AddTransient<IRepository<Waiter, long>, WaiterRepository>()
            .AddTransient<IReadRepository<Order, long>, OrderReadRepository>()
            .AddScoped<IEmployeeService, EmployeeService>()
            .AddScoped<IKitchenService, Services.Implementations.KitchenService>()
            .AddMapping()
            .AddControllers();

        AddMessageBroker(services);

        services.AddSwaggerGen(options =>
        {
            options.SwaggerDoc("v1",
                new OpenApiInfo
                {
                    Title = "Kitchen Service API",
                    Version = "v1"
                });

            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
            options.IncludeXmlComments(xmlPath);
        });
    }

    private void AddMessageBroker(IServiceCollection services)
    {
        var sectionRabbitMq = configuration.GetSection("RabbitMq");

        var host = sectionRabbitMq.GetSection("Host").Value;
        var exchange = sectionRabbitMq.GetSection("Exchange").Value;
        var queue = sectionRabbitMq.GetSection("Queue").Value;

        services.AddRabbitMqMessageBus(host, exchange, queue)
            .AddTransient<SendOrderForCookingMessageHandler>();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }

        app.UseRouting();
        app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

        app.UseSwagger();
        app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "Kitchen API V1"); });

        var messageBus = app.ApplicationServices.GetService<IMessageBus>();
        messageBus!.Subscribe<SendOrderForCookingMessage, SendOrderForCookingMessageHandler>();
    }
}