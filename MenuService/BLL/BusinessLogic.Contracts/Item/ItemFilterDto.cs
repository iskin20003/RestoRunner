﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Contracts.Item
{
    public class ItemFilterDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int Weight { get; set; }
        public int CategoryId { get; set; }
        public int ItemsPerPage { get; set; }
        public int Page { get; set; }
    }
}
