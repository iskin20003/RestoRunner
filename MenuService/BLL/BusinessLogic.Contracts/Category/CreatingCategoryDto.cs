﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Contracts.Category
{
    public class CreatingCategoryDto
    {
        public string Name { get; set; }
    }
}
