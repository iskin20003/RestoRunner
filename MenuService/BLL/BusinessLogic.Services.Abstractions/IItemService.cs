﻿using BusinessLogic.Contracts.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Contracts.Item;

namespace BusinessLogic.Abstractions
{
    public interface IItemService
    {
        Task<ICollection<ItemDto>> GetPagedAsync(int page, int pageSize);
        Task<ItemDto> GetByIdAsync(int id);
        Task<int> CreateAsync(CreatingItemDto creatingItemDto);
        Task UpdateAsync(int id, UpdatingItemDto updatingItemDto);
        Task DeleteAsync(int id);
    }
}
