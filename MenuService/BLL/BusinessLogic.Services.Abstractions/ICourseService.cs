using System.Collections.Generic;
using System.Threading.Tasks;
using BusinessLogic.Contracts.Course;

namespace BusinessLogic.Abstractions
{
    /// <summary>
    /// Cервис работы с курсами (интерфейс).
    /// </summary>
    public interface ICourseService
    {
        Task<ICollection<CourseDto>> GetPagedAsync(int page, int pageSize);
        Task<CourseDto> GetByIdAsync(int id);
        Task<int> CreateAsync(CreatingCourseDto creatingCourseDto);
        Task UpdateAsync(int id, UpdatingCourseDto updatingCourseDto);
        Task DeleteAsync(int id);
    }
}