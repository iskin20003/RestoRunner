using System.Collections.Generic;
using System.Threading.Tasks;
using BusinessLogic.Contracts.Lesson;

namespace BusinessLogic.Abstractions
{
    /// <summary>
    /// Сервис работы с уроками (интерфейс).
    /// </summary>
    public interface ILessonService
    {
        Task<ICollection<LessonDto>> GetPagedAsync(int page, int pageSize);
        Task<LessonDto> GetByIdAsync(int id);
        Task<int> CreateAsync(CreatingLessonDto creatingLessonDto);
        Task UpdateAsync(int id, UpdatingLessonDto updatingLessonDto);
        Task DeleteAsync(int id);
    }
}