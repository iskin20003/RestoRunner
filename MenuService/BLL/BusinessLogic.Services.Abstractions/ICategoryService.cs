﻿using BusinessLogic.Contracts.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Contracts.Category;

namespace BusinessLogic.Abstractions
{
    public interface ICategoryService
    {
        Task<ICollection<CategoryDto>> GetPagedAsync(int page, int pageSize);
        Task<CategoryDto> GetByIdAsync(int id);
        Task<int> CreateAsync(CreatingCategoryDto creatingCategoryDto);
        Task UpdateAsync(int id, UpdatingCategoryDto updatingCategoryDto);
        Task DeleteAsync(int id);
    }
}
