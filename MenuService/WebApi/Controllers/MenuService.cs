﻿using Interfaces;
using Microsoft.AspNetCore.Mvc;
using RestoRunner.Entities;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class MenuService : IMenuService
    {
        public static RestoRunnerDbContext RestoRunnerDbContext = new RestoRunnerDbContext();

        [HttpGet(Name = "Get/{menuItemId}")]
        public MenuItem Get(int menuItemId)
        {
            return RestoRunnerDbContext.MenuItems.FirstOrDefault(x => x.Id == menuItemId);
        }

        [HttpGet(Name = "GetMenuItems")]
        public IEnumerable<MenuItem> GetMenuItems()
        {
            return RestoRunnerDbContext.MenuItems;
        }

        [HttpPost]
        public void Post([FromBody] MenuItem menuItem)
        {
            if (menuItem != null)
            {
                RestoRunnerDbContext.Add(menuItem);
                RestoRunnerDbContext.SaveChanges();
            }  
        }
    }
}
