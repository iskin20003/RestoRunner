﻿using RestoRunner.Entities;

namespace Interfaces
{
    public interface IMenuService
    {
        public MenuItem Get(int menuItemId);
        public IEnumerable<MenuItem> GetMenuItems();
        public void Post(MenuItem menuItem);
    }
}
