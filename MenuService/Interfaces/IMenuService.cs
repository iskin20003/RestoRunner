﻿using RestoRunner.Entities.MenuMicroservice;
using Microsoft.AspNetCore.Mvc;
using Shed.CoreKit.WebApi;
using System.Xml.Linq;

namespace Interfaces
{
    public interface IMenuService
    {
        IEnumerable<MenuItem> Get();

        [HttpGet(Name = "Get/{menuItemId}}")]
        public MenuItem Get(int menuItemId);
    }
}
